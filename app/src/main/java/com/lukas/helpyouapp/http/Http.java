package com.lukas.helpyouapp.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.http.beans.UserBean;

import android.net.Uri;
import android.os.StrictMode;
import android.util.Base64;

public class Http {

	static {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	private static ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	private static final String BASE_URL = "http://192.168.0.105:8080/helpyou-api";

	@SuppressWarnings("serial")
	public static void register(final HttpCallback callback, UserBean user) {
		send(
				"/user",
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"POST",
				user,
				null,
				callback
		);
	}

	@SuppressWarnings("serial")
	public static void login(final HttpCallback callback, UserBean user) {
		send(
				"/user/login",
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"POST",
				user,
				null,
				callback
		);
	}

	@SuppressWarnings("serial")
	public static void newRequest(final HttpCallback callback, RequestBean req) {
		send(
				"/request",
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"POST",
				req,
				null,
				callback
		);
	}

	@SuppressWarnings("serial")
	public static void getClientRequests(final HttpCallback callback) {
		send(
				"/request/client/" + UserInfo.getIntance().getId(),
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"GET",
				null,
				null,
				callback
		);
	}

	@SuppressWarnings("serial")
	public static void getFixerRequests(final HttpCallback callback) {
		send(
				"/request/fixer/" + UserInfo.getIntance().getId(),
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"GET",
				null,
				null,
				callback
		);
	}

	@SuppressWarnings("serial")
	public static void updateRequest(final HttpCallback callback, RequestBean reqBean) {
		send(
				"/request",
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"PUT",
				reqBean,
				null,
				callback
		);
	}

	public static void deleteRequest(final HttpCallback callback, RequestBean reqBean) {
		send(
				"/request/" + reqBean.getId(),
				new HashMap<String, String>(){{
					put("Content-Type", "application/json");
				}},
				"DELETE",
				reqBean,
				null,
				callback
		);
	}

	private static ExecutorService executorService = Executors.newSingleThreadExecutor();

	private static void send(final String path, final Map<String, String> headers, final String method, final Object object, final Map<String, String> queryParams, final HttpCallback callback) {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				HttpResponse httpResponse = new HttpResponse();
				HttpURLConnection connection = null;

				try {
					UserInfo userInfo = UserInfo.getIntance();

					String username = userInfo.getUsername();
					String password = userInfo.getPassword();

					if (username != null && password != null) {
						String usernameAndPassword = username + ":" + password;
						String authorizationHeaderName = "Authorization";
						String authorizationHeaderValue = null;
						authorizationHeaderValue = "Basic " + Base64.encodeToString(usernameAndPassword.getBytes("UTF-8"), Base64.NO_WRAP);
						headers.put(authorizationHeaderName, authorizationHeaderValue);
					}

					String json = null;
					if (object != null) {
						json = ow.writeValueAsString(object);
					}

					Uri.Builder builder = new Uri.Builder();
					if (queryParams != null) {
						for (Map.Entry<String, String> param : queryParams.entrySet()) {
							builder.appendQueryParameter(param.getKey(), param.getValue());
						}
					}

					String targetURL = BASE_URL + path;
					URL url = new URL(targetURL + builder.build().toString());
					connection = (HttpURLConnection)url.openConnection();

					if (method.equals("GET"))
						connection.setDoOutput(false);
					else
						connection.setDoOutput(true);

					connection.setRequestMethod(method);
					for (Map.Entry<String, String> header : headers.entrySet()) {
						connection.setRequestProperty(header.getKey(), header.getValue());
					}
					connection.setConnectTimeout(5000);
					connection.connect();

					if (json != null) {
						DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
						wr.writeBytes(json);
						wr.flush();
						wr.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if(connection != null) {
						try {
							httpResponse.setCode(connection.getResponseCode());
							httpResponse.setText(getResponseText(connection));

							connection.disconnect();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

				callback.perform(httpResponse);
			}
		});
	}

	public static String getResponseText(HttpURLConnection connection) {
		try {
			BufferedReader br;
			if (200 <= connection.getResponseCode() && connection.getResponseCode() <= 299) {
				br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
			} else {
				br = new BufferedReader(new InputStreamReader((connection.getErrorStream())));
			}

			String text = "";
			String aux;

			while ((aux = br.readLine()) != null) {
				text += aux;
			}

			return text;
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

}
