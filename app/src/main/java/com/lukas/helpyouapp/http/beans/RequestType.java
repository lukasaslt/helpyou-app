package com.lukas.helpyouapp.http.beans;

public enum RequestType {
	ELECTRICITY, PLUMBING, COMPUTER, OTHER;
	
	public String asReadable() { 
		switch (this) {
		case ELECTRICITY:
			return "Electricity";
		case PLUMBING:
			return "Plumbing";
		case COMPUTER:
			return "Computer";
		case OTHER:
			return "Other";
		}
		return "";
	}
}
