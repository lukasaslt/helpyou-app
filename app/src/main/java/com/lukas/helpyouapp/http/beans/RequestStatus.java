package com.lukas.helpyouapp.http.beans;

public enum RequestStatus {
	CREATED, ACCEPTED, COMPLETED;
	
	public String asReadable() { 
		switch (this) {
		case CREATED:
			return "Created";
		case ACCEPTED:
			return "Accepted";
		case COMPLETED:
			return "Completed";
		}
		return "";
	}
}
