package com.lukas.helpyouapp.http.beans;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequestBean implements Serializable {
	
	int id;
	String title;
	String desc;
	RequestType requestType;
	RequestStatus requestStatus;
	int userId;
	Integer acceptedUserId;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public RequestType getRequestType() {
		return requestType;
	}
	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}
	public RequestStatus getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Integer getAcceptedUserId() {
		return acceptedUserId;
	}
	public void setAcceptedUserId(Integer acceptedUserId) {
		this.acceptedUserId = acceptedUserId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
