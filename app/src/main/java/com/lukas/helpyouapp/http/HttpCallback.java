package com.lukas.helpyouapp.http;

import com.lukas.helpyouapp.http.beans.HttpResponse;

public interface HttpCallback {
    void perform(HttpResponse response);
}
