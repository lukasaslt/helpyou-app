package com.lukas.helpyouapp;

import com.lukas.helpyouapp.http.beans.RequestType;
import com.lukas.helpyouapp.http.beans.Role;

public class UserInfo {
	
	private int id;
	private String username;
	private String password;
	private Role role;
	private RequestType reqType;
	private static UserInfo userInfo = new UserInfo();
	
	private UserInfo() {
		id = 1;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public static UserInfo getIntance() {
		return userInfo;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public RequestType getReqType() {
		return reqType;
	}

	public void setReqType(RequestType reqType) {
		this.reqType = reqType;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void clear() {
		userInfo = new UserInfo();
	}
	
}
