package com.lukas.helpyouapp.screens;

import com.lukas.helpyouapp.http.beans.RequestType;

public class RequestTypeData {
	
	private RequestType value;
	private String text;

	public RequestTypeData(RequestType value, String text) {
		super();
		this.value = value;
		this.text = text;
	}

	public RequestType getValue() {
		return value;
	}

	public void setValue(RequestType value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
