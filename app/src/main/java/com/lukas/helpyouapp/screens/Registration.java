package com.lukas.helpyouapp.screens;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.Utils;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestType;
import com.lukas.helpyouapp.http.beans.Role;
import com.lukas.helpyouapp.http.beans.UserBean;
import com.lukas.helpyouapp.screens.client.ClientRequests;
import com.lukas.helpyouapp.screens.fixer.FixerRequests;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Registration extends Activity {

	EditText usernameEditText;
	EditText passwordEditText;
	EditText repeatPasswordEditText;
	MaterialBetterSpinner roleSpinner;
	MaterialBetterSpinner reqTypeSpinner;
	Button submitButton;
	String errorText;
	RoleData selectedRole;
	RequestTypeData selectedReqType;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);

		usernameEditText = (EditText) findViewById(R.id.username);
		passwordEditText = (EditText) findViewById(R.id.password);
		repeatPasswordEditText = (EditText) findViewById(R.id.repeatPassword);

		roleSpinner = (MaterialBetterSpinner) findViewById(R.id.roleSpinner);
		final RoleData[] items = new RoleData[] { new RoleData(Role.CLIENT, "Client"), new RoleData(Role.FIXER, "Fixer") };
		ArrayAdapter<RoleData> adapter = new ArrayAdapter<RoleData>(this, android.R.layout.simple_spinner_dropdown_item, items);
		roleSpinner.setAdapter(adapter);
		roleSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            selectedRole = ((RoleData) adapterView.getItemAtPosition(position));

            Role role = selectedRole.getValue();
            MarginLayoutParams params = (MarginLayoutParams) submitButton.getLayoutParams();
            switch (role) {
                case CLIENT:
                    reqTypeSpinner.setVisibility(View.GONE);
                    break;
                case FIXER:
                    reqTypeSpinner.setVisibility(View.VISIBLE);
                    break;
            }
            submitButton.requestLayout();
			}
		});
		
		reqTypeSpinner = (MaterialBetterSpinner) findViewById(R.id.reqTypeSpinner);
	    final RequestTypeData[] itemsReqType = new RequestTypeData[] { 
    		new RequestTypeData(RequestType.ELECTRICITY, "Electricity"),
    		new RequestTypeData(RequestType.PLUMBING, "Plumbing"),
    		new RequestTypeData(RequestType.COMPUTER, "Computer"),
    		new RequestTypeData(RequestType.OTHER, "Other")
		};
		ArrayAdapter<RequestTypeData> adapterReqType = new ArrayAdapter<RequestTypeData>(this, android.R.layout.simple_spinner_dropdown_item, itemsReqType);
		reqTypeSpinner.setAdapter(adapterReqType);
		reqTypeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				selectedReqType = ((RequestTypeData) adapterView.getItemAtPosition(position));
			}
		});

		submitButton = (Button) findViewById(R.id.submit);
		submitButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				String username = usernameEditText.getText().toString();
				String password = passwordEditText.getText().toString();
				String repeatPassword = repeatPasswordEditText.getText().toString();

				if (!validate(username, password, repeatPassword)) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
						}
					});
					return;
				}

				if (selectedRole == null) {
					errorText = "Please select role!";

					Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();

					return;
				}

				if (selectedReqType == null) {
					errorText = "Please select request type!";

					Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();

					return;
				}

				Role role = selectedRole.getValue();
				RequestType reqType = selectedReqType.getValue();

				UserBean user = new UserBean();
				user.setUsername(username);
				user.setPassword(password);
				user.setRole(role);
				user.setReqType(reqType);

				Http.register(new HttpCallback() {
					@Override
					public void perform(HttpResponse response) {
						int responseCode = response.getCode();

						if (responseCode >= 200 && responseCode <= 399) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            startActivity(intent);
                            finish();

							errorText = "Registration successful! You can now login!";
						} else if (responseCode == 409) {
							errorText = "Please choose a different username!";
						} else {
							errorText = "Unexpected error!";
						}

						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
							}
						});
					}
				}, user);
			}
		});
	}
	
	public boolean validate(String username, String password, String repeatPassword) {
		if (username == null || username.equals("") ||
			password == null || password.equals("")) {
			errorText = "Invalid username or password!";
			return false;
		}
		
		if (!password.equals(repeatPassword)) {
			errorText = "Passwords must match!";
			return false;
		}
		
		return true;
	}

	public Activity getActivity() {
		return this;
	}

	class RoleData {

		private Role value;
		private String text;

		public RoleData(Role value, String text) {
			super();
			this.value = value;
			this.text = text;
		}

		public Role getValue() {
			return value;
		}

		public void setValue(Role value) {
			this.value = value;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

}
