package com.lukas.helpyouapp.screens.client;

import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.http.beans.RequestStatus;
import com.lukas.helpyouapp.http.beans.RequestType;
import com.lukas.helpyouapp.screens.RequestTypeData;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewRequest extends Activity {
	
	EditText requestTitle;
	EditText requestDesc;
	MaterialBetterSpinner reqTypeSpinner;
	RequestTypeData selectedItem;
	Button submitButton;
	String errorText;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.newrequest);
	    
	    requestTitle = (EditText) findViewById(R.id.requestTitle);
	    requestDesc = (EditText) findViewById(R.id.requestDesc);
	    submitButton = (Button) findViewById(R.id.requestSubmit);
	    reqTypeSpinner = (MaterialBetterSpinner) findViewById(R.id.requestType);
	    
	    final RequestTypeData[] items = new RequestTypeData[] { 
    		new RequestTypeData(RequestType.ELECTRICITY, "Electricity"),
    		new RequestTypeData(RequestType.PLUMBING, "Plumbing"),
    		new RequestTypeData(RequestType.COMPUTER, "Computer"),
    		new RequestTypeData(RequestType.OTHER, "Other")
		};
		ArrayAdapter<RequestTypeData> adapter = new ArrayAdapter<RequestTypeData>(this, android.R.layout.simple_spinner_dropdown_item, items);
		reqTypeSpinner.setAdapter(adapter);

		reqTypeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				selectedItem = ((RequestTypeData) adapterView.getItemAtPosition(position));
			}
		});
	    
	    submitButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				String title = requestTitle.getText().toString();
				String desc = requestDesc.getText().toString();

				if (title == null) {
					errorText = "Request title can not be empty!";

					Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();

					return;
				}

				if (selectedItem == null) {
					errorText = "Please select request type!";

					Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();

					return;
				}

				RequestType reqType = selectedItem.getValue();

				RequestBean reqBean = new RequestBean();
				reqBean.setTitle(title);
				reqBean.setDesc(desc);
				reqBean.setRequestStatus(RequestStatus.CREATED);
				reqBean.setRequestType(reqType);
				reqBean.setUserId(UserInfo.getIntance().getId());

				Http.newRequest(new HttpCallback() {
					@Override
					public void perform(HttpResponse response) {
						int responseCode = response.getCode();

						if (responseCode >= 200 && responseCode <= 399) {
							errorText = "Request created!";
							Intent intent = new Intent(getActivity(), ClientRequests.class);
							startActivity(intent);
							getActivity().finish();
						} else {
							errorText = "Unexpected error!";
						}

						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
							}
						});
					}
				}, reqBean);
			}
		});
	}
	
	public Activity getActivity() {
		return this;
	}

}
