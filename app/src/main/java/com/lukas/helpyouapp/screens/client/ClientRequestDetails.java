package com.lukas.helpyouapp.screens.client;

import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.http.beans.RequestStatus;
import com.lukas.helpyouapp.http.beans.Role;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ClientRequestDetails extends Activity {

	TextView detailsTitle;
	TextView detailsDesc;
	TextView detailsType;
	TextView detailsStatus;
	Button changeStatusButton;
	Button deleteClientRequestBtn;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.request_details);

		final RequestBean requestBean = (RequestBean) getIntent().getExtras().getSerializable("requestBean");
	    
	    changeStatusButton = (Button) findViewById(R.id.changeStatusButton);
	    changeStatusButton.setVisibility(View.INVISIBLE);

        deleteClientRequestBtn = (Button) findViewById(R.id.delete_client_request);
		boolean visible = UserInfo.getIntance().getRole() == Role.CLIENT
				&& (requestBean.getRequestStatus() == RequestStatus.CREATED ||
					requestBean.getRequestStatus() == RequestStatus.COMPLETED
				);
        deleteClientRequestBtn.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
		deleteClientRequestBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Http.deleteRequest(new HttpCallback() {
					@Override
					public void perform(HttpResponse response) {
						Intent intent = new Intent(getActivity(), ClientRequests.class);
						startActivity(intent);
						getActivity().finish();
					}
				}, requestBean);
			}
		});

    	detailsTitle = (TextView) findViewById(R.id.detailsTitle);
    	detailsDesc = (TextView) findViewById(R.id.detailsDesc);
    	detailsType = (TextView) findViewById(R.id.detailsType);
    	detailsStatus = (TextView) findViewById(R.id.detailsStatus);
    	
    	detailsTitle.append(requestBean.getTitle());
    	detailsDesc.append(requestBean.getDesc());
    	detailsType.append(requestBean.getRequestType().asReadable());
    	detailsStatus.append(requestBean.getRequestStatus().asReadable());
	}

	public Activity getActivity() {
		return this;
	}

}
