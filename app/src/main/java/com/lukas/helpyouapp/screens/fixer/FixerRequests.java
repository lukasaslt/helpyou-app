package com.lukas.helpyouapp.screens.fixer;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.screens.Login;
import com.lukas.helpyouapp.screens.RequestArrayAdapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FixerRequests extends Activity {

	String errorText;
	SwipeRefreshLayout pullToRefresh;
	ListView listView;
	Button logOutBtn;
	List<RequestBean> requests;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fixer_requests);

		logOutBtn = (Button) findViewById(R.id.log_out);
		logOutBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), Login.class);
				startActivity(intent);
				UserInfo.getIntance().clear();
				getActivity().finish();
			}
		});

		listView = (ListView) findViewById(R.id.fixerRequestsList);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				RequestBean requestBean = (RequestBean) listView.getAdapter().getItem(position);
				Intent intent = new Intent(getActivity(), FixerRequestDetails.class);
				intent.putExtra("requestBean", requestBean);
				startActivity(intent);
			}
		});

		pullToRefresh = findViewById(R.id.swipe_container);
		pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				refresh();
			}
		});

		Http.getFixerRequests(new HttpCallback() {
			@Override
			public void perform(HttpResponse response) {
				int responseCode = response.getCode();

				requests = new ArrayList<RequestBean>();
				if (responseCode >= 200 && responseCode <= 399) {
					String allRequestsJson;
					try {
						allRequestsJson = response.getText();
						if (allRequestsJson != null) {
							ObjectMapper mapper = new ObjectMapper();
							requests = mapper.readValue(allRequestsJson, new TypeReference<List<RequestBean>>() {
							});
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					errorText = "Unexpected error!";

					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
						}
					});
				}

				getActivity().runOnUiThread(new Runnable() {
					public void run() {
					listView.setAdapter(new RequestArrayAdapter(listView.getContext(), requests));
					}
				});

				pullToRefresh.setRefreshing(false);
			}
		});
	}

	private void refresh() {

	}

	public Activity getActivity() {
		return this;
	}

}
