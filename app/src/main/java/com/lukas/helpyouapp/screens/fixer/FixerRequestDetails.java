package com.lukas.helpyouapp.screens.fixer;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.http.beans.RequestStatus;
import com.lukas.helpyouapp.http.beans.Role;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FixerRequestDetails extends Activity {

	TextView detailsTitle;
	TextView detailsDesc;
	TextView detailsType;
	TextView detailsStatus;
	Button changeStatusButton;
	RequestBean requestBean;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.request_details);
	    
    	requestBean = (RequestBean) getIntent().getExtras().getSerializable("requestBean");
    	
    	detailsTitle = (TextView) findViewById(R.id.detailsTitle);
    	detailsDesc = (TextView) findViewById(R.id.detailsDesc);
    	detailsType = (TextView) findViewById(R.id.detailsType);
    	detailsStatus = (TextView) findViewById(R.id.detailsStatus);
    	changeStatusButton = (Button) findViewById(R.id.changeStatusButton);
    	
    	updateView(UserInfo.getIntance().getRole(), requestBean.getRequestStatus());
    	
    	changeStatusButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				switch (requestBean.getRequestStatus()) {
				case CREATED:
					requestBean.setRequestStatus(RequestStatus.ACCEPTED);
					break;
				case ACCEPTED:
					requestBean.setRequestStatus(RequestStatus.COMPLETED);
					requestBean.setAcceptedUserId(UserInfo.getIntance().getId());
					break;
				default:
					break;
				}

				Http.updateRequest(new HttpCallback() {
					@Override
					public void perform(final HttpResponse response) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							int responseCode = response.getCode();

							String errorText = "";
							if (responseCode >= 200 && responseCode <= 399) {
								updateView(UserInfo.getIntance().getRole(), requestBean.getRequestStatus());
								errorText = "Request updated!";
							} else {
								errorText = "Unexpected error!";
							}

							Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
						}
					});
					}
				}, requestBean);
			}
			
    	});
    	
    	detailsTitle.append(requestBean.getTitle());
    	detailsDesc.append(requestBean.getDesc());
    	detailsType.append(requestBean.getRequestType().asReadable());
    	detailsStatus.append(requestBean.getRequestStatus().asReadable());
	}
	
	public void updateView(Role role, RequestStatus reqStatus) {
		switch (role) {
    	case CLIENT:
    		changeStatusButton.setVisibility(View.INVISIBLE);
    		break;
    	case FIXER:
    		switch (reqStatus) {
    		case CREATED:
    			changeStatusButton.setText("Accept");
    			break;
    		case ACCEPTED:
    			changeStatusButton.setText("Complete");
    			break;
    		case COMPLETED:
    			changeStatusButton.setText("Complete");
    			changeStatusButton.setEnabled(false);
    			break;
    		}
    	}
	}

	public Activity getActivity() {
		return this;
	}

}
