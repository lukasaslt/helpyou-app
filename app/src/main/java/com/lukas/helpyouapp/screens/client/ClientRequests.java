package com.lukas.helpyouapp.screens.client;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.RequestBean;
import com.lukas.helpyouapp.screens.Login;
import com.lukas.helpyouapp.screens.RequestArrayAdapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ClientRequests extends Activity {
	
	String errorText;
	SwipeRefreshLayout pullToRefresh;
	ListView listView;
    List<RequestBean> requests;
    Button addClientRequestBtn;
    Button logOutBtn;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_requests);

        addClientRequestBtn = (Button) findViewById(R.id.add_client_request);
        addClientRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewRequest.class);
                startActivity(intent);
            }
        });

        logOutBtn = (Button) findViewById(R.id.log_out);
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Login.class);
                startActivity(intent);
                UserInfo.getIntance().clear();
                getActivity().finish();
            }
        });

        listView = (ListView) findViewById(R.id.clientRequestsList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RequestBean requestBean = (RequestBean) listView.getAdapter().getItem(position);
                Intent intent = new Intent(getActivity(), ClientRequestDetails.class);
                intent.putExtra("requestBean", requestBean);
                startActivity(intent);
            }
        });

        pullToRefresh = findViewById(R.id.swipe_container);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        refresh();
    }

	private void refresh() {
        Http.getClientRequests(new HttpCallback() {
            @Override
            public void perform(HttpResponse response) {
                int responseCode = response.getCode();

                requests = new ArrayList<RequestBean>();
                if (responseCode >= 200 && responseCode <= 399) {
                    String allRequestsJson;
                    try {
                        allRequestsJson = response.getText();
                        if (allRequestsJson != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            requests = mapper.readValue(allRequestsJson, new TypeReference<List<RequestBean>>() {
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    errorText = "Unexpected error!";
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        listView.setAdapter(new RequestArrayAdapter(listView.getContext(), requests));
                    }
                });

                pullToRefresh.setRefreshing(false);
            }
        });
	}
	
	public Activity getActivity() {
		return this;
	}

}
