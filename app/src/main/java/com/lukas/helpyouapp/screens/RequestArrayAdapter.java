package com.lukas.helpyouapp.screens;

import java.util.List;

import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.http.beans.RequestBean;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RequestArrayAdapter extends ArrayAdapter<RequestBean> {
	private final Context context;
	private final List<RequestBean> values;

	public RequestArrayAdapter(Context context, List<RequestBean> values) {
		super(context, R.layout.request_item, values);
		this.context = context;
		this.values = values;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		RequestBean reqBean = values.get(position);
		View rowView = inflater.inflate(R.layout.request_item, parent, false);
		TextView textViewName = (TextView) rowView.findViewById(R.id.list_item_name);
		textViewName.setText(reqBean.getTitle());
		
		TextView textViewStatus = (TextView) rowView.findViewById(R.id.list_item_status);
		textViewStatus.setText(reqBean.getRequestStatus().asReadable());
		switch (reqBean.getRequestStatus()) {
		case CREATED:
			textViewStatus.setTextColor(Color.RED);
			break;
		case ACCEPTED:
			textViewStatus.setTextColor(Color.YELLOW);
			break;
		case COMPLETED:
			textViewStatus.setTextColor(Color.GREEN);
			break;
		default:
			break;
		}

		return rowView;
	}
}
