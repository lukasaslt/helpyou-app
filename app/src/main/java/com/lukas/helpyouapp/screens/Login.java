package com.lukas.helpyouapp.screens;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukas.helpyouapp.R;
import com.lukas.helpyouapp.UserInfo;
import com.lukas.helpyouapp.http.Http;
import com.lukas.helpyouapp.http.HttpCallback;
import com.lukas.helpyouapp.http.beans.HttpResponse;
import com.lukas.helpyouapp.http.beans.UserBean;
import com.lukas.helpyouapp.screens.client.ClientRequests;
import com.lukas.helpyouapp.screens.fixer.FixerRequests;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {

	EditText usernameEditText;
	EditText passwordEditText;
	Button loginButton;
	String errorText;
	TextView registerLink;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		usernameEditText = (EditText) findViewById(R.id.usernameLogin);
		passwordEditText = (EditText) findViewById(R.id.passwordLogin);

		registerLink = (TextView) findViewById(R.id.register);
		registerLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), Registration.class);
				startActivity(intent);
			}
		});

		loginButton = (Button) findViewById(R.id.login);
		loginButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				String username = usernameEditText.getText().toString();
				String password = passwordEditText.getText().toString();

				UserBean user = new UserBean();
				user.setUsername(username);
				user.setPassword(password);

				UserInfo userInfo = UserInfo.getIntance();
				userInfo.setUsername(user.getUsername());
				userInfo.setPassword(user.getPassword());

				Http.login(new HttpCallback() {
					@Override
					public void perform(HttpResponse response) {
						int responseCode = response.getCode();

						if (responseCode >= 200 && responseCode <= 399) {
							try {
								String userJson = response.getText();
								if (userJson != null) {
									ObjectMapper mapper = new ObjectMapper();
									UserBean userBean = mapper.readValue(userJson, UserBean.class);

									UserInfo userInfo = UserInfo.getIntance();
									userInfo.setRole(userBean.getRole());
									userInfo.setId(userBean.getId());
									userInfo.setReqType(userBean.getReqType());

									Intent intent;
									switch (userBean.getRole()) {
										case CLIENT:
											intent = new Intent(getActivity(), ClientRequests.class);
											startActivity(intent);
											break;
										case FIXER:
											intent = new Intent(getActivity(), FixerRequests.class);
											startActivity(intent);
											break;
									}

									finish();
								}

								errorText = "Success!";
							} catch (IOException e) {
								e.printStackTrace();
								errorText = "Unexpected error!";
							}
						} else if (responseCode == 401) {
							errorText = "Invalid username or password!";
						} else {
							errorText = "Unexpected error!";
						}

						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
							}
						});
					}
				}, user);
			}
		});
	}

	public Activity getActivity() {
		return this;
	}

}
